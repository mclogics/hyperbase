<?php

/**
 * @package     :HyperBase
 * @subpackage  :System
 * @version     :7.0
 * @author      :Dyutiman Chakraborty <dc@mclogics.com>
 */

namespace Plugin\System\Preference;
use System, RuntimeException;

class Plugin
{
    protected $kernel;

    /**
     * Initialize plguin
     */
    public function __construct(System\Kernel $kernel)
    {
        $kernel->loader()->registerNamespace('Model', __DIR__);

        $kernel->addMethod('set_preference', function($key, $value, $user=false) {
            $user_id = $user==false?user('id'):$user->id;
            $object = \Model\Preference::find_or_create_by_key_name_and_user_id($key, $user_id);
            $object->key_value = $value;
            $object->save();
            return true;
        });

        $kernel->addMethod('get_preference', function($key, $default=false, $user=false) {
            $user_id = $user==false?user('id'):$user->id;
            $object = \Model\Preference::find_by_key_name_and_user_id($key, $user_id);
            if($object instanceof \Model\Preference) {
                return $object->key_value;
            }
            return $default;
        });

        return true;
    }

}