<?php

namespace ActiveRecord;
use System;

abstract class Field extends \ActiveRecord\Model
{
    /**
     * Field type definations
     *
     * @var int
     */
    static $field_type_text     = 1;
    static $field_type_date     = 2;
    static $field_type_time     = 3;
    static $field_type_datetime = 4;

    /**
     * Description of internal fileds.
     *
     * @var array
     */
    static $field_description = array();

    /**
     * If set to false, then any existing tag can
     * be deleted. else only empty tags can be deleted,
     * Empty Tag = tags with no record attached.
     *
     * @var bool
     */
    static $allow_tag_delete_only_if_empty = false;

    /**
     * If set to true, then new tags are created on assignment, if required,
     * else tags need to be created manually before assignment.
     *
     * @var bool
     */
    static $allow_auto_creation_of_tag_on_assignment = false;

    /**
     * If set to false the validation rules of internal fields can not
     * be dynamically modifed.
     *
     * @var bool
     */
    static $allow_validation_overwriting_of_internal_fields = false;

    /**
     * Validation rules
     *
     * @var array
     */
    static $validates_size_of           = array();
    static $validates_presence_of       = array();
    static $validates_length_of         = array();
    static $validates_inclusion_of      = array();
    static $validates_exclusion_of      = array();
    static $validates_format_of         = array();
    static $validates_numericality_of   = array();
    static $validates_uniqueness_of     = array();
    static $validates_is_email          = array();

    /**
     * Types of available validations
     *
     * @var array
     */
    static $validation_types = array(
        'size_of', 'presence_of', 'length_of', 'inclusion_of', 'exclusion_of', 'format_of',
        'numericality_of', 'uniqueness_of', 'is_email'
    );

    /**
     * Add a new custom field to the DB Table
     *
     * @param array $field
     * @return array
     * @throws \Exception
     */
    public static function add_field(array $field=array())
    {
        self::check_schema();
        $connection = parse_url(\Kernel()->config('app.plugin.system.db.connections.production'));
        $conn = mysqli_connect(
            $connection['host'],
            $connection['user'],
            $connection['pass'],
            str_replace('/', '', $connection['path'])
        );
        if(isset($field['name'])) {
            $field = array($field);
        }
        $response = array();
        foreach($field as $fld) {
            if(!isset($fld['name'])) {
                throw new \Exception('Field name is required for custom field');
            }
            if(\Model\Field::count(array('conditions'   => array('table_name = ? AND name = ?', static::$table_name, $fld['name']))) > 0) {
                throw new \Exception('Field name must be unique for each custom field');
            }
            if(isset(static::$schema[$fld['name']])) {
                throw new \Exception('Field name must be unique for each custom field');
            }
            $fld['name'] = str_replace(' ', '_', strtolower($fld['name']));
            \Model\Field::create(array_merge(array('table_name' => static::$table_name), $fld));
            $response[] = $fld['name'];
            $sql = 'ALTER TABLE `'.static::$table_name.'` ADD `'.$fld['name'].'` '.$fld['data_type'];
            //___debug($sql);
            mysqli_query($conn, $sql);
        }
        mysqli_close($conn);
        return $response;
    }

    /**
     * Update an existing field.
     * Interbal fileds can also be updated with certain limitations.
     *
     * @param $name
     * @param array $field
     */
    public static function update_field($name, array $field=array())
    {
        self::check_schema();
        if(isset(static::$schema[$name])) {
            $meta = \Model\Field::find_or_create_by_table_name_and_name(static::$table_name, $name);
        } else {
            $meta = \Model\Field::find_by_table_name_and_name(static::$table_name, $name);
        }
        if(isset($field['label'])) {
            $meta->label = $field['label'];
        }
        if(isset($field['description'])) {
            $meta->description = $field['description'];
        }
        if(isset($field['required'])) {
            $meta->required = $field['required'];
        }
        if(isset($field['meta'])) {
            $meta->meta = json_encode($field['meta']);
        }
        $meta->save();
        return $meta;
    }

    /**
     * Get the details of an existing field
     *
     * @param $name
     * @return object|\stdClass
     * @throws \Exception
     */
    public static function get_field($name)
    {
        self::check_schema();
        $fld = \Model\Field::find_by_table_name_and_name(static::$table_name, $name);
        if($fld) {
            return $fld;
        }
        if(isset(static::$schema[$name])) {
            if(isset(static::$field_description[$name])) {
                return (object) static::$field_description[$name];
            } else {
                $object = new \stdClass();
                $object->label = ucwords(str_replace('_', ' ', $name));
                $object->description = null;
                $object->required = false;
                return $object;
            }
        }
        throw new \Exception('Unknowen field ' . $name . ' for table ' . static::$table_name);
    }

    /**
     * Returns all the custom_field and meta info of other fields
     * of the table.
     *
     * @return mixed
     * @throws \Exception
     */
    public static function get_all_fields()
    {
        self::check_schema();
        return \Model\Field::find_all_by_table_name(static::$table_name);
    }

    /**
     * Remove an existing filed.
     * Internal fileds can not be deleted
     *
     * @param $name
     * @return bool
     * @throws \Exception
     */
    public static function remove_field($name)
    {
        self::check_schema();
        if(isset(static::$schema[$name])) {
            throw new \Exception('Internal fields can not be deleted');
        }
        $field = \Model\Field::find_by_table_name_and_name(static::$table_name, $name);
        if(!$field) {
            throw new \Exception('The filed '. $name . ' do not exist');
        }
        $connection = parse_url(\Kernel()->config('app.plugin.system.db.connections.production'));
        $conn = mysqli_connect(
            $connection['host'],
            $connection['user'],
            $connection['pass'],
            str_replace('/', '', $connection['path'])
        );
        $sql = 'ALTER TABLE `'.static::$table_name.'` DROP `'.$name.'`';
        mysqli_query($conn, $sql);
        $field->delete();
        return true;
    }

    /**
     * Creates a new tag, which can be attached to the
     * rows of this table
     *
     * @param $name
     * @return mixed
     */
    public static function create_tag($name)
    {
        return \Model\Tag::find_or_create_by_table_name_and_name(static::$table_name, $name);
    }

    /**
     * Returns a list of tag available for this table.
     *
     * @param $name
     * @return mixed
     */
    public static function tag_list($name)
    {
        return \Model\Tag::find_all_by_table_name(static::$table_name);
    }

    /**
     * Delete an existing tag of this table
     *
     * @param $name
     * @return bool
     * @throws \Exception
     */
    public static function delete_tag($name)
    {
        $tag = \Model\Tag::find_by_table_name_and_name(static::$table_name, $name);
        if($tag) {
            if(static::$allow_tag_delete_only_if_empty==true) {
                if(\Model\TagRelation::count_by_tag_id($tag->id) == 0) {
                    $tag->delete();
                    return true;
                }
                return false;
            }
            $tag->delete();
            return true;
        }
        throw new \Exception('Tag ' . $name . ' is not present');
    }

    /**
     * Search records from a table based on tags
     *
     * <code>
     *    \Model\Table::search_by_tag(array(
     *       'has_all'          => array(), // All of the mentioned tag is present
     *       'has_any'          => array(), // Any of the mentioned tag is present
     *       'has_none'         => array(), // None of the mentioned tag is present
     *       'has_missing_any'  => array()  // Atlast one of the mentioned tag is missing
     *    ));
     * </code>
     *
     * @param array $params
     * @return array
     */
    public static function search_by_tag(array $params=array())
    {
        /**
         * SELECT contacts.*
         * FROM contacts, tag_relations
         * WHERE contacts.id = tag_relations.record_id
         * AND tag_relations.tag_id IN (3,2)
         * GROUP BY contacts.id
         * HAVING COUNT(tag_relations.tag_id) = 2
         */
        if(isset($params['has_all'])) {
            $has_all = array();
            foreach($params['has_all'] as $ha) {
                $tag = \Model\Tag::find_by_table_name_and_name(static::$table_name, $ha);
                if($tag) { $has_all[] = $tag->id; }
            }
        }

        /**
         * SELECT contacts.*
         * FROM contacts, tag_relations
         * WHERE contacts.id = tag_relations.record_id
         * AND tag_relations.tag_id IN (3,2)
         * GROUP BY contacts.id
         */
        if(isset($params['has_any'])) {
            $has_any = array();
            foreach($params['has_any'] as $ha) {
                $tag = \Model\Tag::find_by_table_name_and_name(static::$table_name, $ha);
                if($tag) { $has_any[] = $tag->id; }
            }
        }

        /**
         * SELECT contacts.*
         * FROM contacts, tag_relations
         * WHERE contacts.id = tag_relations.record_id
         * AND tag_relations.tag_id NOT IN (3,2)
         * GROUP BY contacts.id
         */
        if(isset($params['has_none'])) {
            $has_none = array();
            foreach($params['has_none'] as $ha) {
                $tag = \Model\Tag::find_by_table_name_and_name(static::$table_name, $ha);
                if($tag) { $has_none[] = $tag->id; }
            }
        }

        /**
         * SELECT contacts.*
         * FROM contacts, tag_relations
         * WHERE contacts.id = tag_relations.record_id
         * AND tag_relations.tag_id IN (3,2)
         * GROUP BY contacts.id
         * HAVING COUNT(tag_relations.tag_id) < 2
         */
        if(isset($params['has_missing_any'])) {
            $has_missing_any = array();
            foreach($params['has_missing_any'] as $ha) {
                $tag = \Model\Tag::find_by_table_name_and_name(static::$table_name, $ha);
                if($tag) { $has_missing_any[] = $tag->id; }
            }
        }

        return array();
    }


    /**
     * @param $key
     * @param $value
     * @return array
     */
    public static function search_by_attribute($key, $value)
    {
        return array();
    }

    /**
     * An internal function to check that a schema array
     * for the model exists.
     *
     * @return bool
     * @throws \Exception
     */
    protected static function check_schema()
    {
        if(isset(static::$schema) && is_array(static::$schema) && count(static::$schema) > 0) {
            return true;
        }
        throw new \Exception('A vaild DB schema is required but seems to be missing');
    }

    /**
     * Sets an attribute for an record of the table.
     *
     * @param $name
     * @param $value
     * @return $this
     */
    public function set_attrib($name, $value)
    {
        $attrib = \Model\Attribute::find_or_create_by_table_name_and_record_id_and_name(static::$table_name, $this->id, $name);
        if(is_array($value)) {
            $attrib->data = json_encode($value);
            $attrib->is_json = 1;
            $attrib->save();
            return $this;
        }
        if($value==null) {
            $attrib->delete();
            return $this;
        }
        $attrib->data = json_encode($value);
        $attrib->is_json = 1;
        $attrib->save();
        return $this;
    }

    /**
     * Gets the value of an existing attribute
     * if the attribute is not present, then NULL is returned.
     *
     * @param $name
     * @return mixed|null
     */
    public function get_attrib($name)
    {
        $attrib = \Model\Attribute::find_by_table_name_and_record_id_and_name(static::$table_name, $this->id, $name);
        if($attrib) {
            if($attrib->is_json == 1) {
                return json_decode($attrib->data, true);
            } else {
                return $attrib->data;
            }
        }
        return null;
    }

    /**
     * Removes an existing attribute from a record
     * Same as setting the value of the attribute to null
     *
     * @param $name
     * @return Field
     */
    public function remove_attrib($name)
    {
        return $this->setAttrib($name, null);
    }

    /**
     * Attaches a tag to the currect record of the model object
     *
     * @param $name
     * @return bool
     * @throws \Exception
     */
    public function tag($name)
    {
        if(static::$allow_auto_creation_of_tag_on_assignment == true) {
            $tag = \Model\Tag::find_or_create_by_table_name_and_name(static::$table_name, $name);
        } else {
            $tag = \Model\Tag::find_by_table_name_and_name(static::$table_name, $name);
        }
        if($tag instanceof \Model\Tag) {

            $tagRelation = \Model\TagRelation::find_or_create_by_table_name_and_record_id_and_tag_id(
                static::$table_name, $this->id, $tag->id
            );
            if($tagRelation instanceof \Model\TagRelation) {
                return true;
            }
            return false;
        }
        throw new \Exception('Unable to find or create tag ' . $name);
    }

    /**
     * Returns a list of all the tag relations attached to
     * this Model Record object
     *
     * @return mixed
     */
    public function get_tags()
    {
        return \Model\TagRelation::find_all_by_table_name_and_record_id(
            static::$table_name, $this->id
        );
    }

    /**
     * Detaches an existing tag which is already attached to the record.
     *
     * @param $name
     * @return bool
     * @throws ActiveRecordException
     */
    public function remove_tag($name)
    {
        $tag = \Model\Tag::find_by_table_name_and_name(static::$table_name, $name);
        if($tag) {
            $tagRelation = \Model\TagRelation::find_by_table_name_and_record_id_and_tag_id(
                static::$table_name, $this->id, $tag->id
            );
            if($tagRelation instanceof \Model\TagRelation) {
                $tagRelation->delete();
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * Sets custom validation rules
     *
     * @return bool
     * @throws \Exception
     */
    protected function set_custom_validations()
    {
        self::check_schema();
        foreach(self::get_all_fields() as $fld) {
            if(!isset(static::$schema[$fld->name]) || (isset(static::$schema[$fld->name])
                && static::$allow_validation_overwriting_of_internal_fields==true)) {
                $meta = json_decode($fld->meta_data, true);
                foreach(static::$validation_types as $type) {
                    if(isset(static::$schema[$fld->name])) {
                        $this->remove_field_validation($fld->name, $type);
                    }
                    if (isset($meta['validation'][$type])) {
                        $validation_type = 'validates_'. $type;
                        static::${$validation_type}[] = array_merge(
                            array($fld->name),
                            is_array($meta['validation'][$type]) ? $meta['validation'][$type] : array()
                        );
                    }
                }
            }
        }
        return true;
    }

    /**
     * Removes field validation for an existing field
     * if already defined.
     *
     * @param $name
     * @param $type
     * @return bool
     */
    protected function remove_field_validation($name, $type)
    {
        $validation_type = 'validates_'. $type;
        $current_validations = static::${$validation_type};
        static::${$validation_type} = array();
        foreach($current_validations as $vattrib) {
           if($vattrib[0] != $name) {
               static::${$validation_type}[] = $vattrib;
           }
        }
        return true;
    }

}





























