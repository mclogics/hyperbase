<?php

namespace Model;
use ActiveRecord, System;

class Field extends \ActiveRecord\Model
{
    /**
     * Name of the table related to the User Model
     *
     * @var string
     */
    static $table_name = 'plugin_system_db_field_fields';

    /**
     * @var array
     */
    static $schema = array(
        'id'          => 'bigint(250) NOT NULL AUTO_INCREMENT',
        'table_name'  => 'text NOT NULL',
        'name'        => 'text NOT NULL',
        'label'       => 'text NOT NULL',
        'description' => 'text NOT NULL',
        'data_type'   => 'text NOT NULL',
        'meta_data'   => 'text NOT NULL',
        'created_at'  => 'timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
        'updated_at'  => 'timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\''
    );

}





























