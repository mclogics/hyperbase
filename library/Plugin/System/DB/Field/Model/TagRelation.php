<?php

namespace Model;
use ActiveRecord, System;

class TagRelation extends \ActiveRecord\Model
{
    /**
     * Name of the table related to the User Model
     *
     * @var string
     */
    static $table_name = 'plugin_system_db_field_tag_relations';

    static $belongs_to = array(
        array('tag')
    );

    /**
     * @var array
     */
    static $schema = array(
        'id'          => 'bigint(250) NOT NULL AUTO_INCREMENT',
        'table_name'  => 'text NOT NULL',
        'record_id'   => 'bigint(250) NOT NULL',
        'tag_id'      => 'bigint(250) NOT NULL',
        'created_at'  => 'timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
        'updated_at'  => 'timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\''
    );

}





























