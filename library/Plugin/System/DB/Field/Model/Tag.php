<?php

namespace Model;
use ActiveRecord, System;

class Tag extends \ActiveRecord\Model
{
    /**
     * Name of the table related to the User Model
     *
     * @var string
     */
    static $table_name = 'plugin_system_db_field_tags';

    static $has_many = array(
        array('tagrelations', 'class_name' => 'TagRelation')
    );

    /**
     * @var array
     */
    static $schema = array(
        'id'          => 'bigint(250) NOT NULL AUTO_INCREMENT',
        'table_name'  => 'text NOT NULL',
        'name'        => 'text NOT NULL',
        'created_at'  => 'timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
        'updated_at'  => 'timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\''
    );

}





























