<?php

namespace Model;
use ActiveRecord, System;

class Attribute extends \ActiveRecord\Model
{
    /**
     * Name of the table related to the User Model
     *
     * @var string
     */
    static $table_name = 'plugin_system_db_field_attributes';

    /**
     * @var array
     */
    static $schema = array(
        'id'          => 'bigint(250) NOT NULL AUTO_INCREMENT',
        'table_name'  => 'text NOT NULL',
        'record_id'   => 'bigint(250) NOT NULL',
        'name'        => 'text NOT NULL',
        'data'        => 'text NOT NULL',
        'is_json'     => 'int(1) NOT NULL DEFAULT \'0\'',
        'created_at'  => 'timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
        'updated_at'  => 'timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\''
    );

}





























