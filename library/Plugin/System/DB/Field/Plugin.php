<?php

/**
 * @package     :HyperBase
 * @subpackage  :System
 * @version     :7.0
 * @author      :Dyutiman Chakraborty <dc@mclogics.com>
 */

namespace Plugin\System\DB\Field;
use System, RuntimeException;

/**
 * Field Plugin
 * Allows a Model to do the followings:
 * =====================================
 * 1. Add and manage meta-data to existing base fields
 * 2. Allows to add and manage custom field to the DB table
 * 3. Allows to add additional attribute to each data row
 * 4. Allows to tag data rows
 * @todo: 5. Provides a search functionality for tags.
 * @todo: 6. Perform validation on data
 *
 */
class Plugin
{
    protected $kernel;

    /**
     * Initialize plguin
     */
    public function __construct(System\Kernel $kernel)
    {
        $kernel->loader()->registerNamespace('ActiveRecord', __DIR__);
        $kernel->loader()->registerNamespace('Model', __DIR__);

        //___debug($kernel->loader());
    }

}