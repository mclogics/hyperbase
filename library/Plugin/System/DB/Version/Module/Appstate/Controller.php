<?php

namespace Module\Appstate;

use App,
    System,
    Model;

/**
 * Index Module
 *
 * Extends from base Application controller so custom functionality can be added easily
 * lib/App/Module/ControllerAbstract
 */
class Controller extends App\Module\ControllerAbstract
{
    protected $authentication = false;

    public function versionAction(System\Request $request)
    {
        $this->disableLayout();
        return $this->kernel->getAppVersion();
    }

    public function statusAction(System\Request $request)
    {
        $this->disableLayout();
        return array();
    }

}




















