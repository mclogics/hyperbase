<?php

/**
 * All HyperBase code is Copyright 2001 - 2012 by the original authors.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program as the file LICENSE.txt; if not, please see
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt.
 *
 * HyperBase includes works under other copyright notices and distributed
 * according to the terms of the GNU General Public License or a compatible
 * license.
 *
 */

namespace System;

abstract class Job
{
    protected $kernel;
    protected $params = array();
    protected $job;
    protected $stack_params = array();
    protected $max_percentage;

    public function setParams(array $params=array()) { $this->params = $params; }
    public function setStackParams(array $params=array()) { $this->stack_params = $params; }
    public function setMaxPercentage($n) { $this->max_percentage = $n; }
    public function setJob(\Model\Job $job) { $this->job = $job; }
    protected function pre_run() { return true; }
    protected function post_run() { return true; }

    abstract protected function run();

    public function __construct()
    {
        $this->kernel = \Kernel();
    }

    public function init()
    {
        try {
            $pre_run = $this->pre_run();
        } catch(\Exception $e) {
            $this->setLogMessage($e->getMessage());
            $pre_run = false;
        }
        if($pre_run==false) {
            return false;
        }
        try {
            $return = $this->run();
        } catch(\Exception $e) {
            $this->setLogMessage($e->getMessage(), 'error');
            return false;
        }

        $this->post_run();
        return $return;
    }

    public function exec($cmd)
    {
        exec('nohup ' . $cmd .' >> '. $this->job->log());
        return true;
    }

    public function param($key, $default=null)
    {
        if(isset($this->params[$key])) {
            return $this->params[$key];
        }
        return $default;
    }

    public function setPercetage($n) //5
    {
        $an = ($this->max_percentage/100)*$n;
        $this->job->percentage = $this->job->percentage + $an;
        $this->job->save();
        return $an;
    }

    public function autoCompletePercentage($callback, $target, $maxunit=0)
    {
        $job_lock_file = '/tmp/'.$this->job->id.'.job.lock';
        $an = ($this->max_percentage/100)*$target;
        file_put_contents($job_lock_file, json_encode(array($an, $maxunit)));
        $url = 'https://'.$_SERVER['argv'][1].'/job/'.$this->job->id.'/incweb';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_exec ($ch);
        $return = call_user_func_array($callback, array());
        unlink($job_lock_file);
        sleep(1);
        return $return;
    }

    public function setLogMessage($message, $type='info', $message_append="\n", $filter=false)
    {
        return $this->job->log($message, $type, $message_append, $filter);
    }

}