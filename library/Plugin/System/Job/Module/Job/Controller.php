<?php

namespace Module\Job;

class Controller extends \App\Module\ControllerAbstract
{
    protected $skip_authentication = array('exec', 'incpercent', 'incweb');

    /**
     * @param \System\Request $request
     * @return bool
     */
    public function execAction(\System\Request $request)
    {
        if(!$request->isCli()) {
            return false;
        }
        \Model\Job::find($request->param('item'))->exec();
        return array();
    }

    public function postMethod(\System\Request $request)
    {
        return $this->kernel->createJob(array(
            'application' =>  $request->post('application', 0),
            'jobs'        =>  $request->post('jobs', array())
        ));
    }

    public function scriptAction(\System\Request $request)
    {
        $this->disableLayout();
        $this->view->jobid = $request->param('item');
        return $this->view;
    }

    public function viewAction(\System\Request $request)
    {
        $job = \Model\Job::find($request->param('item'));
        if(isset($_SESSION['__job'][$job->id])) {
            unset($_SESSION['__job'][$job->id]);
        }
        $this->view->job = $job;
        return $this->view;
    }

    public function logprefAction(\System\Request $request)
    {
        if($request->get('state') == 'hide') {
            $this->kernel->set_preference('show_live_job_log', 0);
        } else {
            $this->kernel->set_preference('show_live_job_log', 1);
        }
        return array();
    }

    public function castAction(\System\Request $request)
    {
        $job = \Model\Job::find($request->param('item'));
        //$app = \Model\Application::find($job->application_id);

        if(!isset($_SESSION['__job'][$job->id])) {
            $_SESSION['__job'][$job->id] = '';
        }

        //$log = str_replace($app->name.'_', '', file_get_contents($job->log()));
        $log = file_get_contents($job->log());
        $newLog = str_replace($_SESSION['__job'][$job->id], '', $log);
        $_SESSION['__job'][$job->id] = $log;

        if(!file_exists('/proc/'.$job->pid) && $job->percentage != 100) {
            $job->jobfailed();
            return array(
                'status'    => $job->state_text,
                'state'     => $job->state,
                'log'       => '',
                'percent'   => $job->percentage
            );
        }

        if(in_array($job->state, array(JOB_STATE_FAILED, JOB_STATE_SUCCESS))) {
        //if(in_array($job->state, array(JOB_STATE_FAILED, JOB_STATE_SUCCESS, JOB_STATE_RUNNING))) {
            unset($_SESSION['__job'][$job->id]);
        }
        return array(
            'status'    => $job->state_text,
            'state'     => $job->state,
            'log'       => $newLog,
            'console_append' => $job->console_append,
            'percent'   => $job->percentage
        );
    }

    public function incwebAction(\System\Request $request)
    {
        $this->kernel->runCLI('job/'.$request->param('item').'/incpercent');
        return array();
    }

    public function incpercentAction(\System\Request $request)
    {
        if(!$request->isCli()) {
            return false;
        }
        $job = \Model\Job::find($request->param('item'));
        $job_lock_file = '/tmp/'.$job->id.'.job.lock';
        if(!file_exists($job_lock_file)) {
            return array();
        }
        if(!isset($target_percentage)) {
            $jdata = json_decode(file_get_contents($job_lock_file), true);
            $target_percentage = $jdata[0];
            $max_unit = $jdata[1];
        }
        while(true) {
            $total_left = $target_percentage - $job->percentage;
            $inc = round(($total_left/10), 2);
            if($max_unit != 0 && $inc > $max_unit) {
                $inc = $max_unit;
            }
            $job->percentage = $job->percentage + $inc;
            $job->save();
            if(!file_exists($job_lock_file)) {
                break;
            }
            sleep(1);
        }
        $job->percentage = $target_percentage;
        $job->save();
        return array();
    }

}




















