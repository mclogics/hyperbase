<h4 id="job-console-heading" style="margin: 0;padding: 0;margin-bottom:10px;"><?=$job->state_text?></h4>
<div style="float:right;margin-top:-27px;">
    <a class="toggle-log" href="#"></a>
</div>
<div id="job-live-progress-bar" class="progress" style="background-color:#eee">
    <div class="progress-bar progress-bar-striped active" role="progressbar"
         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
    </div>
</div>

<pre id="job-live-screen"></pre>
<pre id="job-live-screen-append" style="height:38px;"></pre>

<div id="console-end-box-success" class="console-end-box alert alert-success">
    <div style="float:left;width:743px;padding:6px;">
        This job has been successfully completed, and will close automatically in <span class="job-console-close-in">30 seconds</span>.
    </div>
    <div style="float:left;padding-left:20px">
        <a href="#" class="btn btn-primary btn-sm">Close Job</a>
    </div>
    <div style="clear:both;"></div>
</div>
<div id="console-end-box-failed" class="console-end-box alert alert-danger" style="color: #cc0000">
    <div style="float:left;width:743px;padding:6px;">
        This job has failed, and will close automatically in <span class="job-console-close-in">30 seconds</span>.
    </div>
    <div style="float:left;padding-left:20px">
        <a href="#" class="btn btn-primary btn-sm btn-danger">Close Job</a>
    </div>
    <div style="clear:both;"></div>
</div>

<script>
    $(document).ready(function() {
        var endURI = false;
        var jobpole = setInterval(function() {
            $.get('/job/<?=$job->id?>/cast', function(data) {
                $('#job-live-progress-bar div').css('width', data.percent+'%');

                if(data.log == '') {
                    if($('#job-live-screen .onscreen-wait').length < 1) {
                        $('#job-live-screen').append('<span class="onscreen-wait"></span>');
                    }
                    $('#job-live-screen .onscreen-wait').append('.');
                } else {
                    $('#job-live-screen .onscreen-wait').remove();
                    $('#job-live-screen').append(data.log);
                }

                if(typeof(data.console_append) == 'undefined' || data.console_append == null) {
                    $('#job-live-screen-append').hide();
                } else {
                    $('#job-live-screen-append').show()
                    $('#job-live-screen-append').html(data.console_append);
                }

                $('#job-console-heading').html(data.status);
                if(data.state == <?=JOB_STATE_FAILED?> || data.state == <?=JOB_STATE_SUCCESS?>) {
                    $('#job-live-screen .onscreen-wait').remove();
                    if(data.state == <?=JOB_STATE_SUCCESS?>) {
                        //setTimeout(function () {
                            $('#console-end-box-success').show();
                        //}, 1000);
                    }
                    if(data.state == <?=JOB_STATE_FAILED?>) {
                        //setTimeout(function () {
                            $('#console-end-box-failed').show();
                        //}, 1000);
                    }
                    $('#job-live-progress-bar .progress-bar').removeClass('active');
                    clearInterval(jobpole);
                    if (typeof(data.enduri) != 'undefined') {
                        endURI = data.enduri;
                    }
                    var closeIn = 30;
                    var closeInterval = setInterval(function() {
                        if(closeIn == 1) {
                            if (typeof(data.enduri) != 'undefined') {
                                window.location = data.enduri;
                            } else {
                                //location.reload();
                            }
                            clearInterval(closeInterval);
                        }
                        closeIn = closeIn - 1;
                        if(closeIn < 2) {
                            var unit = 'second';
                        } else {
                            var unit = 'seconds';
                        }
                        $('.job-console-close-in').html(closeIn + ' ' + unit);
                    }, 1000);
                }
                var elem = document.getElementById('job-live-screen');
                elem.scrollTop = elem.scrollHeight;
            }, 'json');
        }, 1000);
        $('.console-end-box .btn').click(function() {
            if(endURI == false) {
                location.reload();
            } else {
                window.location = endURI;
            }
            return false;
        });
        $('.toggle-log').click(function() {
            if($('#job-live-screen').is(':visible')) {
                $('.toggle-log').html('Show Logs');
                $('#job-live-screen').hide();
                $.get('/job/<?=$job->user_id?>/logpref?state=hide', function() {});
            } else {
                $('.toggle-log').html('Hide Logs');
                $('#job-live-screen').show();
                $.get('/job/<?=$job->id?>/logpref?state=show', function() {});
            }
            var elem = document.getElementById('job-live-screen');
            elem.scrollTop = elem.scrollHeight;
            return false;
        });
        <?php if(\Kernel()->get_preference('show_live_job_log', 0)==1) { ?>
        $('.toggle-log').html('Hide Logs');
        $('#job-live-screen').show();
        <?php } else { ?>
        $('.toggle-log').html('Show Logs');
        $('#job-live-screen').hide();
        <?php } ?>
    });
</script>