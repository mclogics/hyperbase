//console.log('Job Loaded : <?=$jobid?>');
$(document).ready(function () {
    $('#navigation-load-modal .modal-body').html('Please wait. Loading job <?=$jobid?>...');
    $('#navigation-load-modal').addClass('bs-modal-lg');
    $('#navigation-load-modal .modal-dialog').addClass('modal-lg');
    $('#navigation-load-modal .modal-body').css('padding', '20px');
    $('#navigation-load-modal').modal({
        backdrop: 'static',
        keyboard: false
    });
    $.get('/job/<?=$jobid?>', function(data) {
        $('#navigation-load-modal .modal-body').html(data);
    });
});