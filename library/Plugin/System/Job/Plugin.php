<?php

/**
 * @package     :HyperBase
 * @subpackage  :System
 * @version     :7.0
 * @author      :Dyutiman Chakraborty <dc@mclogics.com>
 */

namespace Plugin\System\Job;
use System, RuntimeException;

define('JOB_STATE_CREATED', 0);
define('JOB_STATE_RUNNING', 9);
define('JOB_STATE_FAILED',  8);
define('JOB_STATE_SUCCESS', 1);

/**
 * Layout Plugin
 * Wraps layout template around content result from main dispatch loop
 */
class Plugin
{
    protected $kernel;

    /**
     * Initialize plguin
     */
    public function __construct(System\Kernel $kernel)
    {
        $kernel->loader()->registerNamespace('Model', __DIR__);
        $kernel->loader()->registerNamespace('Module', __DIR__);
        $kernel->loader()->registerNamespace('System', __DIR__);
        $kernel->loader()->registerNamespace('Job', $kernel->config('app.path.root'));

        $kernel->addMethod('createJob', function(array $params=array()) {
            if(!isset($params['application'])) {
                //throw new \Exception('Application must be specified');
                $params['application'] = 0;
            }
            if(!isset($params['jobs']) || count($params['jobs'])==0) {
                throw new \Exception('At least one job must be specified to be performed on the application');
            }
            return \Model\Job::run($params['application'], $params['jobs'], $params['state_text']);
        });

        //Inject Active Job
        $kernel->events('application')->bind('controller_user_loaded', 'plugin_system_job', function($controller) {
            if(isset($controller->user->id)) {
                $job = \Model\Job::find_by_user_id_and_state_or_state(
                    $controller->user->id, JOB_STATE_CREATED, JOB_STATE_RUNNING);
                if ($job instanceof \Model\Job) {
                    $controller->view->helper('Head')->script('job/' . $job->id . '/script.js');
                }
            }
        });

        return true;
    }

}