<?php

namespace Model;
use ActiveRecord, System;

class Job extends \ActiveRecord\Model
{
    /**
     * Name of the table related to the User Model
     *
     * @var string
     */
    static $table_name = 'plugin_system_job_job';

    static $schema = array(
      "id"              => "bigint(250) NOT NULL AUTO_INCREMENT",
      "application_id"  => "text NOT NULL",
      "user_id"         => "bigint(250) NOT NULL DEFAULT '0'",
      "created_at"      => "timestamp NULL DEFAULT NULL",
      "updated_at"      => "timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP",
      "started_at"      => "timestamp NULL DEFAULT NULL",
      "completed_at"    => "timestamp NULL DEFAULT NULL",
      "percentage"      => "text",
      "state"           => "int(1) NOT NULL DEFAULT '0'",
      "data"            => "text NOT NULL",
      "for_account"     => "bigint(250) NOT NULL",
      "pid"             => "int(11) NOT NULL DEFAULT '0'",
      "state_text"      => "text NOT NULL",
      "state_info"      => "text NOT NULL",
      "console_append"  => "text"
    );

    /**
     * @param $application
     * @param array $jobs
     * @return ActiveRecord\Model
     * @throws \Exception
     */
    static public function run($application, array $jobs=array(), array $state_text=array())
    {
        if(count($jobs)==0) {
            throw new \Exception('At least one job must be specified to be performed on the application');
        }
        $object = self::create(array(
            'application_id'    => $application==0?0:$application->id,
            'user_id'           => user('id', 0),
            'state'             => JOB_STATE_CREATED,
            'data'              => json_encode($jobs),
            'state_text'        => isset($state_text['start'])?$state_text['start']:'',
            'state_info'        => json_encode($state_text),
            'percentage'        => 0,
            'for_account'       => \Kernel()->request()->post('for_account', user('id', 0))
        ));
        $object->pid = \Kernel()->runCLI('job/' . $object->id . '/exec');
        $object->save();
        return $object;
    }

    /**
     * @return bool
     */
    public function exec()
    {
        $this->started_at = date('Y-m-d H:i:s');
        $this->state = JOB_STATE_RUNNING;
        $this->save();
        $failed = false;
        $jobdata = json_decode($this->data, true);
        $state_info = json_decode($this->state_info, true);
        $maxPercentile = floor(100/count($jobdata));
        foreach($jobdata as $job => $params) {
            if(isset($params['state_text'])) {
                $this->state_text = $params['state_text'];
                $this->save();
            }
            $class = "\\Job\\".$job;
            $j = new $class;
            $j->setMaxPercentage($maxPercentile);
            $j->setParams($params);
            $j->setStackParams($jobdata);
            $j->setJob($this);
            $result = $j->init();
            if($result == false) {
                $failed = true;
                break;
            }
        }
        //exit;
        $this->state = $failed==true?JOB_STATE_FAILED:JOB_STATE_SUCCESS;
        $this->percentage = 100;
        $this->completed_at = date('Y-m-d H:i:s');
        if($failed==true) {
            if (isset($state_info['failed'])) {
                $this->state_text = $state_info['failed'];
            }
        } else {
            if (isset($state_info['end'])) {
                $this->state_text = $state_info['end'];
            }
        }
        $this->save();
        return true;
    }

    public function jobfailed()
    {
        $this->state = JOB_STATE_FAILED;
        @exec('kill '.$this->pid);
        $this->percentage = 100;
        $this->completed_at = date('Y-m-d H:i:s');
        $this->state_text = 'Oops! Something went wrong';
        $this->save();
    }

    public function log($message=false, $type='info', $message_append="\n", $filter=false)
    {
        $log_path = \Kernel()->config('app.plugin.system.job.log.path', '/tmp').'/'.$this->id.'.log';
        if(!file_exists($log_path)) {
            file_put_contents($log_path,'');
        }
        if($message==false) {
            return $log_path;
        }

        if(is_array($message)) {
            $wait = isset($message[1])?$message[1]:null;
            $message = $message[0];
        }

        if(is_callable($filter)) {
            $message = call_user_func_array($filter, array($message));
        }

        if($type != 'info') {
            $message = '<span class="console-message important '.$type.'">'.$message.'</span>';
        }

        file_put_contents($log_path, $message.$message_append, FILE_APPEND);
        if(isset($wait) && $wait != null) {
            sleep($wait);
        }
        return true;
    }

}





























