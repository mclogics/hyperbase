<?php

/**
 * @package     :HyperBase
 * @subpackage  :System
 * @version     :7.0
 * @author      :Dyutiman Chakraborty <dc@mclogics.com>
 */

namespace Plugin\System\Bootstrap;
use System, RuntimeException;

/**
 * Layout Plugin
 * Wraps layout template around content result from main dispatch loop
 */
class Plugin
{
    protected $kernel;

    /**
     * Initialize plguin
     */
    public function __construct(System\Kernel $kernel)
    {
        //$f = array();
        foreach($kernel->loader()->getNamespacePaths('Module') as $path) {
            foreach(scandir($path . '/Module') as $mod) {
                if(!in_array($mod, array('.', '..'))) {
                    //$f[]= $path . '/Module/'.$mod;
                    if(file_exists($path . '/Module/'.$mod . '/manifest.php')) {
                        include_once $path . '/Module/'.$mod . '/manifest.php';
                    }
                }
            }
            if(file_exists($path.'/bootstrap.php')) {
                include_once $path.'/bootstrap.php';
            }
        }

    }

}