<?php

/**
 * @package     :HyperBase
 * @subpackage  :System
 * @version     :7.0
 * @author      :Dyutiman Chakraborty <dc@mclogics.com>
 */

namespace Plugin\System\User;
use System, RuntimeException;

/**
 * Layout Plugin
 * Wraps layout template around content result from main dispatch loop
 */
class Plugin
{
    protected $kernel;

    protected $default_schema = 'uniuser';

    /**
     * Initialize plguin
     */
    public function __construct(System\Kernel $kernel)
    {
        $scheme = $kernel->config('app.plugin.system.user.schema', $this->default_schema);
        $schema_path = realpath(__DIR__ . '/schema/'.$scheme);
        $kernel->loader()->registerNamespace('Model', $schema_path);
        $kernel->loader()->registerNamespace('App', $schema_path);
    }

}