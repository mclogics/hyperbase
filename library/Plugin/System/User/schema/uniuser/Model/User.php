<?php

namespace Model;
use ActiveRecord, System;

class User extends \ActiveRecord\Model
{
    /**
     * Name of the table related to the User Model
     *
     * @var string
     */
    static $table_name = 'plugin_system_user_users';

    static $schema = array(
      "id"              => "bigint(250) NOT NULL AUTO_INCREMENT",
      "first_name"      => "text NOT NULL",
      "last_name"       => "text NOT NULL",
      "email"           => "text NOT NULL",
      "password"        => "text NOT NULL",
      "created_at"      => "timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP",
      "updated_at"      => "timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'",
      "status"          => "int(2) NOT NULL",
      "data"            => "text NOT NULL",
      "apikey"          => "text NOT NULL",
      "last_login"      => "timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'",
      "user_type"       => "int(1) NOT NULL DEFAULT '0'",
      "plan"            => "text NOT NULL",
    );

}





























