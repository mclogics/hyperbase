<?php
namespace App\Module;
use System, Model, App;

/**
 * Base abstract 'App' module
 * Extend all your application controllers from this and add custom functionality here
 */
abstract class ControllerAbstract  extends System\Module\ControllerAbstract
{
    protected $authentication = true;
    
    protected $skip_authentication = array();

    public $user=null;
          
    public function __construct(System\Kernel $kernel)
    {
        parent::__construct($kernel);
        if($this->authentication == true) {
            $this->authentication();
            $this->kernel->events('application')->trigger('controller_user_loaded', array($this));
        }
    }

    /**
     * Authenticates access
     *
     * @return bool
     * @throws \Exception
     */
    protected function authentication()
    {
        if(in_array($this->kernel->request()->param('action'), $this->skip_authentication)) {
            return true;
        }

        if(!$this->kernel->session()->get('user')) {
            if($this->kernel->request()->post('auth', false) != false) {
                $auth = $this->kernel->request()->post('auth');
                $userArray = Model\User::find_all_by_email_and_password($auth['username'], md5($auth['password']));
                if(count($userArray)==0) {
                   $user = null;
                } else if(count($userArray)>1) {
                    $user = null;
                } else {
                    $user = $userArray[0];
                }
                if($user instanceof \Model\User) {
                    if($user->status == 0) {
                        $this->showLogin('Your account has been suspended. Please contact support for more details');
                    }
                    $this->kernel->session()->set('user', array('id' => $user->id));
                    $user->last_login = date('Y-m-d H:i:s');
                    $user->save();
                    $this->user = $user;

                    //___debug($_SESSION);

                    if($this->kernel->session()->get('login_landing') != false) {
                        $landing = $this->kernel->session()->get('login_landing');
                        unset($_SESSION['login_landing']);
                        $this->kernel->redirect($landing);
                    } else {
                        unset($_SESSION['login_landing']);
                        $this->kernel->redirect();
                    }
                    return true;
                } else {
                    $this->showLogin('The email or the password you have provided is incorrect. Please check your email and password and try again');
                }
            } else {
                $this->showLogin();
            }
        } else {
            $this->user = Model\User::find($_SESSION['user']['id']);
            return true;
        }
    }

    /**
     * @param Model\Consumer $consumer
     * @return Model\Consumer
     */
    protected function loginAs(\Model\User $user)
    {
        if($user->status == 0) {
            //$this->showLogin('Your account has been suspended. Please contact support for more details');
        }
        $this->kernel->session()->set('user', array('id' => $user->id));
        $this->user = $user;
        return $user;
    }

    /**
     * @param bool $message
     * @param string $type
     */
    protected function showLogin($message=false,$type='error')
    {
        //$this->view->helper('Head')->stylesheet('access.css');
        if($this->kernel->request()->isAjax()==true) {
            echo '<script> window.location = "/"; </script>';
            exit;
        }
        $this->kernel->session()->set('login_landing', $_SERVER['REQUEST_URI']);
        include_once $this->kernel->config('app.path.layouts') . '/app.login.html.php';
        exit;
    }


}
