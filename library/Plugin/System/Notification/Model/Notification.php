<?php

namespace Model;
use ActiveRecord, System;
use Nette\Mail\Message;

class Notification extends \ActiveRecord\Model
{
    /**
     * Name of the table related to the User Model
     *
     * @var string
     */
    static $table_name = 'plugin_system_notification_notification';

    static $image_uri = 'http://wpforever.com/wp-content/email';

    static $schema = array(
      "id"          => "bigint(250) NOT NULL AUTO_INCREMENT",
      "user_id"     => "bigint(250) NOT NULL DEFAULT '0'",
      "emailto"     => "text NOT NULL",
      "emailfrom"   => "text NOT NULL",
      "subject"     => "text NOT NULL",
      "data"        => "text NOT NULL",
      "content"     => "text NOT NULL",
      "created_at"  => "timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP",
      "updated_at"  => "timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'",
      "send_at"     => "timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'",
      "status"      => "int(1) NOT NULL DEFAULT '0'",
      "is_html"     => "int(1) NOT NULL DEFAULT '0'",
      "attachments" => "text NOT NULL",
      "protocol"    => "text NOT NULL"
    );

    static public function send($to, $options, $protocol='default')
    {
        if(is_array($to)) {
            $notify = array();
            foreach($to as $send_to) {
                $notify[] = self::send($send_to, $options, $protocol);
            }
            return $notify;
        }
        $arguments = array();
        if(!isset($options['subject'])) {
            throw new \Exception('Subject is required for sending mail');
        }
        if(!isset($options['template'])) {
            throw new \Exception('Mail template is required for sending mail');
        }
        if(stripos($options['template'],'.html')!== false) {
            $arguments['is_html'] = 1;
        } else {
            $arguments['is_html'] = 0;
        }
        $arguments['emailfrom'] = \Kernel()->config('app.plugin.system.noification.sender', 'system@hyperbase');
        if($to instanceof \Model\User) {
            $arguments['user_id'] = $to->id;
            $arguments['emailto'] = ''.$to->first_name.' '.$to->last_name.' <'.$to->email.'>';
        } else {
            $arguments['emailto'] = $to;
        }
        $arguments['protocol'] = $protocol;
        $arguments['subject'] = $options['subject'];
        $arguments['data'] = json_encode(isset($options['vars'])?$options['vars']:array());
        $template_path = \Kernel()->config('system.path.root')
            .'/'.\Kernel()->config('app.plugin.system.notification.template_location')
            .'/'.$options['template'];
        if(!file_exists($template_path)) {
            throw new \Exception('Invalid mail template path');
        }
        $content = process_template(
            file_get_contents($template_path),
            isset($options['vars'])?$options['vars']:array()
        );

        $doc = new \DOMDocument();
        @$doc->loadHTML($content);
        $tags = $doc->getElementsByTagName('img');
        //$images = array();
        foreach ($tags as $tag) {
            //$images[] = $tag->getAttribute('src');
            $image_path = \Kernel()->config('app.plugin.system.notification.image_uri').'/'.$tag->getAttribute('src');
            $content = str_replace($tag->getAttribute('src'), $image_path, $content);

        }
        //___debug($content);
        $arguments['content'] = $content;

        if(isset($options['attachments']) && is_array($options['attachments'])) {
            $arguments['attachments'] = json_encode($options['attachments']);
        } else {
            $arguments['attachments'] = json_encode(array());
        }
        return self::create($arguments)->push();
    }

    public function push()
    {
        if(!\Kernel()->request()->isCli()) {
            \Kernel()->runCLI('exsend/'.$this->id.'/process');
            return $this;
        }
        $mail = new Message;
        $mail->setFrom($this->emailfrom);
        $mail->setSubject($this->subject);
        $mail->addTo($this->emailto);
        if($this->is_html == 1) {
            $mail->setHTMLBody($this->content);
        } else {
            $mail->setBody($this->content);
        }
        $attachments = json_decode($this->attachments, true);
        foreach($attachments as $attachment) {
            if(file_exists($attachment)) {
                $mail->addAttachment($attachment);
            }
        }
        $mailer = new \Nette\Mail\SmtpMailer(
            \Kernel()->config('app.plugin.system.notification.protocols.'.$this->protocol)
        );
        $this->send_at = date('U');
        $mailer->send($mail);
        $this->status = 1;
        $this->save();
        return $this;
    }

}





























