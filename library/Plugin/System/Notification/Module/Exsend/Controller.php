<?php

namespace Module\Exsend;

class Controller extends \App\Module\ControllerAbstract
{
    protected $skip_authentication = array('process');

    public function processAction(\System\Request $request)
    {
        if(!$request->isCli()) {
            return false;
        }
        \Model\Notification::find($request->param('item'))->push();
        return array();
    }
}




















