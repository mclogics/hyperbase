<?php
namespace System\Module;

/**
 * Base Module Mapper
 * 
 * Dependencies:
 *	- Spot
 *
 * @package system
 * @link http://systemframework.com/
 * @license http://www.opensource.org/licenses/bsd-license.php
 */
abstract class Mapper extends \Spot_Mapper
{
    // Used for future extensibility for global module changes
}
