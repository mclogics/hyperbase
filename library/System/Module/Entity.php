<?php
namespace System\Module;

/**
 * Base mapper entity for individual items
 * 
 * Dependencies:
 *	- Spot
 *
 * @package system
 * @link http://systemframework.com/
 * @license http://www.opensource.org/licenses/bsd-license.php
 */
class Entity extends \Spot_Entity
{
    
}
