#!/usr/bin/env bash

apt-get update
apt-get install -y apt-transport-https ca-certificates glusterfs-client
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" > /etc/apt/sources.list.d/docker.list
apt-get update

apt-get install -y docker-engine=1.10.3-0~trusty


sudo docker run \
  --volume=/:/rootfs:ro \
  --volume=/var/run:/var/run:rw \
  --volume=/sys:/sys:ro \
  --volume=/var/lib/docker/:/var/lib/docker:ro \
  --detach=true \
  --name=cadvisor \
  google/cadvisor:latest \
  -storage_driver=influxdb \
  -storage_driver_host=159.203.99.27:8086 \
  -storage_driver_db=db548529\
  -storage_driver_user=wpfeuser \
  -storage_driver_password=kjS54Tyk65fd \
  -storage_driver_secure=false


#mount -t glusterfs gluster1:/datapoint /ftpdata