#!/usr/bin/env bash

sudo apt-get install -y software-properties-common

sudo add-apt-repository ppa:gluster/glusterfs-3.5
sudo apt-get update

sudo apt-get install -y glusterfs-server

=============

gluster peer probe storageX

=============

sudo gluster volume create datastore replica 2 transport tcp storage1:/opt/datapool storage2:/opt/datapool force

sudo mount -t glusterfs storage1:/datastore /mnt/storage


=============

10.0.0.1  storage2
10.0.0.2   storage1