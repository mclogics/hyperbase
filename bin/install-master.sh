#!/usr/bin/env bash

apt-get update
apt-get install -y apt-transport-https ca-certificates
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" > /etc/apt/sources.list.d/docker.list
apt-get update

#1.11.0-0~trusty
apt-get install -y docker-engine=1.10.3-0~trusty

sudo docker run -d --restart=always -p 8080:8080 \
    -e CATTLE_DB_CATTLE_MYSQL_HOST=mysql \
    -e CATTLE_DB_CATTLE_MYSQL_PORT=3306 \
    -e CATTLE_DB_CATTLE_MYSQL_NAME=cattle \
    -e CATTLE_DB_CATTLE_USERNAME=rcatuser \
    -e CATTLE_DB_CATTLE_PASSWORD=r87QzKXRUEB5Qtjy \
    rancher/server

sudo docker run -d --restart=always -v /mnt/storage/mysql:/var/lib/mysql --name="rancher_controller" rancher/server

sudo docker run -d --restart=always  --name="rancher_controller" rancher/server

#docker run -d rancher/glusterfs:v0.2.1


docker run --name wpforever-mysql --restart=always -v /mnt/storage/wpforever/mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=gHmtW1902 -d mysql:5.5

docker run --name phpmyadmin  --restart=always -d --link wpforever-mysql:db phpmyadmin/phpmyadmin