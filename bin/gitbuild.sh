#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd ${DIR}
cd ../
/usr/bin/git pull --all

/usr/bin/php ${DIR}/dbvc.php

# su -s /bin/sh admin -c 'sh /home/admin/application/bin/gitbuild.sh'