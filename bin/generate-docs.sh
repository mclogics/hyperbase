#!/usr/bin/env bash

yes | ./apigen.phar generate --source ./ --destination documentations --exclude vendor --exclude public --exclude documentations