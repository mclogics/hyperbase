<?php

    final class Monitor
    {
        static $net_dev = 'eth0';
        static $profileing_interval = 30;
        static $report_url = 'https://cloud.wpforever.com/monitor';
        static $key_file = '/wpforever.key';
        static $storage_path = '/var/www/html';
        static $wp_config_file = '/var/www/html/wp-config.php';

        protected $app_id = null;

        /**
         * @return null|string
         */
        protected function getKey()
        {
            if(!file_exists(static::$key_file)) {
                return null;
            }
            return file_get_contents(static::$key_file);
        }

        /**
         * @return null|string
         */
        protected function getApplicationID()
        {
            if($this->app_id==null) {
                $cdata_a = explode('define(\'DB_USER\',', file_get_contents(static::$wp_config_file));
                $cdata_b = explode('\');', $cdata_a[1]);
                $this->app_id = trim(str_replace('\'dbuser_', '', $cdata_b[0]));
            }
            return $this->app_id;
        }

        /**
         * @return array
         */
        protected static function memory()
        {
            $free = shell_exec('free');
            $free = (string)trim($free);
            $free_arr = explode("\n", $free);
            $mem = explode(" ", $free_arr[1]);
            $mem = array_filter($mem);
            $mem = array_merge($mem);
            return array(
                'total'  => $mem[1],
                'used'  => $mem[2],
            );
        }

        /**
         * @return array
         */
        protected static function storage()
        {
            return array(
                'total' => disk_total_space(static::$storage_path),
                'free'  => disk_free_space(static::$storage_path)
            );
        }

        protected static function debug($var)
        {
            print_r($var); exit;
        }

        protected static function getDockerMetaData($id)
        {
            $i = exec('docker ps --no-trunc | grep ' . $id);
            $x = array();
            foreach(preg_split("/[ \t]{2,}/", $i) as $key => $value) {
                if($key == 0) { $x['id'] = trim($value); }
                if($key == 1) { $x['image'] = trim($value); }
                if($key == 2) { $x['cmd'] = trim($value); }
                if($key == 3) { $x['created'] = trim($value); }
                if($key == 4) { $x['status'] = trim($value); }
                if($key == 5) { $x['ports'] = trim($value); }
                if($key == 6) { $x['name'] = trim($value); }
            }
            return $x;
        }

        protected static function container()
        {
            $lfile = '/tmp/stats.temp';
            if(file_exists($lfile)) {
                unlink($lfile);
            }
            exec('/usr/bin/docker stats --no-stream >> ' . $lfile);
            $i = file($lfile);
            unlink($lfile);
            $x = array();
            foreach($i as $k => $stat) {
                if($k==0) { continue; }
                foreach(preg_split("/[ \t]{2,}/", $stat) as $key => $value) {
                    if($key == 0) { $x[$k]['host'] = trim($value); }
                    if($key == 1) { $x[$k]['cpu'] = trim($value); }
                    if($key == 2) { $x[$k]['mem_usage'] = trim($value); }
                    if($key == 3) { $x[$k]['mem'] = trim($value); }
                    if($key == 4) { $x[$k]['new_io'] = trim($value); }
                    if($key == 5) { $x[$k]['block_io'] = trim($value); }
                }
                $x[$k] = array_merge($x[$k], self::getDockerMetaData($x[$k]['host']));
            }
            return $x;
        }

        /**
         * @return array
         */
        protected static function network()
        {
            return array(
                'rx'   => array(
                    'bytes'     => trim(file_get_contents('/sys/class/net/'.static::$net_dev.'/statistics/rx_bytes')),
                    'packets'   => trim(file_get_contents('/sys/class/net/'.static::$net_dev.'/statistics/rx_packets')),
                    'errors'    => trim(file_get_contents('/sys/class/net/'.static::$net_dev.'/statistics/rx_errors')),
                    'dropped'   => trim(file_get_contents('/sys/class/net/'.static::$net_dev.'/statistics/rx_dropped'))
                ),
                'tx'   => array(
                    'bytes'     => trim(file_get_contents('/sys/class/net/'.static::$net_dev.'/statistics/tx_bytes')),
                    'packets'   => trim(file_get_contents('/sys/class/net/'.static::$net_dev.'/statistics/tx_packets')),
                    'errors'    => trim(file_get_contents('/sys/class/net/'.static::$net_dev.'/statistics/tx_errors')),
                    'dropped'   => trim(file_get_contents('/sys/class/net/'.static::$net_dev.'/statistics/tx_dropped'))
                ),
            );
        }

        /**
         * @return array
         */
        protected function profile()
        {
            //$ipr = str_replace(array('  ', 'inet','scope','global','eth0', '/16'), '', shell_exec('ip add | grep inet\ 10.'));
            return array(
                'hostname'   => trim(exec('hostname')),
                'key'        => $this->getKey(),
                'cpu'        => sys_getloadavg(),
                'memory'     => self::memory(),
                'storage'    => self::storage(),
                'net'        => self::network(),
                'containers' => self::container()
            );

        }

        /**
         * @param $profile
         * @return bool
         */
        protected function send($profile)
        {
            $url = static::$report_url;
            $data = array('m' => $profile);

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $result = curl_exec ($ch);
            curl_close ($ch);

            if($this->getKey()==null) {
                $rdata = json_decode($result, true);
                if(isset($rdata['key'])) {
                    //print_r($rdata); exit;
                    file_put_contents(static::$key_file, $rdata['key']);
                    //echo "New Key Saved \n";
                }
            }
            //print_r($result); echo "\n";
            return true;
        }

        /**
         * @return bool
         */
        public function run()
        {
            //self::debug($this->profile());
            $this->send(json_encode($this->profile()));
            return true;
        }
    }

    $i = new Monitor();
    $i->run();
    exit;
















