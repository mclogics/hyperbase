<?php

$root_path = __DIR__.'/..';

include_once $root_path . '/functions.php';
include_once $root_path . '/library/System/Spyc.php';

$schame_path = $root_path . '/resources/schema';

if(!file_exists($root_path.'/resources/db.version.yml')) {
    //copy($root_path.'/resources/db.yml', $root_path.'/resources/db.version.yml');
    _debug('ERROR:: The file resources/db.version.yml do not exist.'."\n");
}

$meta = \System\Spyc::YAMLLoad(realpath($root_path.'/resources/db.version.yml'));

//___debug($meta);

$current_version = $meta['dbversion'];
$new_db_version = $current_version;

foreach(scandir($schame_path) as $sqlFile) {
    if(!in_array($sqlFile, array('.','..'))) {
        if($meta['dbversion'] <  str_replace('.sql', '', $sqlFile)) {
            $command='mysql -h' .$meta['db']['host'] .' -u' .$meta['db']['user'] .' -p' .$meta['db']['pass'] .' ' .$meta['db']['name'] .' < ' . $schame_path . '/' . $sqlFile;
            exec($command,$output=array(),$worked);
            $new_db_version = str_replace('.sql', '', $sqlFile);
            echo "DB version has been set to " . $new_db_version . "\n";
        }
    }
}

if($new_db_version > $current_version) {
    $meta['dbversion'] = $new_db_version;
    unlink($root_path . '/resources/db.version.yml');
    file_put_contents($root_path . '/resources/db.version.yml', \System\Spyc::YAMLDump($meta));
}