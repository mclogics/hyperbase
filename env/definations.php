<?php
/**
 * Definations
 *
 * PHP version 5.5.9
 *
 * Some low level defination about the the deployment of the framework.
 *
 * @category   Platform
 * @package    Functions
 * @author     Dyutiman Chakraborty <dc@mclogics.com>
 * @copyright  2014 - 2015, HB
 * @license    https://licenses.domain.com/psl-1.0.txt Proprietary Service Licence ver. 1.0
 *
 * @link       https://framework.local
 */

/**
 * Defining Permission Level Types
 */
define('APP_LEVEL', 1);         //Application Level Permission
define('USR_LEVEL', 2);         //User Level Permission
define('DEFAULT_PERMISSION_LEVEL', APP_LEVEL);

/**
 * Define the types of deployment
 */
define('SAAS', 1);                  //SaaS Platform with support for multiple application with multiple instances
define('CONTAINER', 2);             //SaaS Platform with support for one application with multiple instances
define('APPLICATION', 3);           //SaaS Platform with support for one application with one instances
define('DEPLOYMENT_MODE', APPLICATION);

define('USER_TYPE_ADMIN', 1);
define('USER_TYPE_CLIENT', 5);

