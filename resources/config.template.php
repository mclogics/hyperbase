<?php

// Configuration
$app = array();
$app['name'] = 'HyperBase'; 					// Name of the Application
$app['version'] = '3.0';
$app['title'] = 'HyperBase';			// Web Browser Titkle for the Application
$app['theme']  = 'default';  				// Theme to use for this application
$app['url']['rewrite'] = true; 				// Enable or disbale url rewrite
$app['debug'] = false;						// Set debug mode
$app['mode']['development'] = true;	    	// Set development mode
$app['session']['lifetime'] = 28000;		// Set session lifetime
$app['identity'] = 'HyperBase 3.0';    		// Mail identity of the application
$app['protocol'] = 'https';
$app['i18n'] = array(						// LOCAL SETTINGS
    'charset' => 'UTF-8',
    'language' => 'en_US',
    'timezone' => 'Asia/Calcutta',
    'date_format' => 'M d, Y',
    'time_format' => 'H:i:s'
);
$app['php']['cli']['exec'] = '/usr/bin/php';
$app['table']['pagination']['perpage'] = 25;

// Load Required Plugins
$app['plugins'] = array(
    'System_Layout',
    'System_DB',
    'System_DB_Field',
    'System_DB_Version',
    'System_Bootstrap',
    'System_User',
    'System_Job',
    'System_Preference',
    'System_Notification'
);


$app['plugin']['system']['db'] = array(
    'connections'  => array(
        'production' => 'mysql://{{db_user}}:{{db_password}}@{{db_host}}/{{db_name}}'
    ),
    'default_connection' => 'production'
);

$app['plugin']['system']['user'] = array(
    'schema'    => 'uniuser'
);


$app['plugin']['system']['notification'] = array(
    'protocols' => array(
        'default'   => array(
            'host'      => 'smtp.sendgrid.net',
            'username'  => 'wpforevercloudsys====',
            'password'  => 'H4XVkR3aD5ZXwmAzEBtnutPc=====',
            'secure'    => 'ssl',
            'port'      => 465
        )
    ),
    'template_location' => 'application/emails',
    'sender'            => 'Hyperbase Application <hyperbase@logicalclouds.com>',
    'image_uri'         => 'http://wpforever.com/wp-content/email'
);