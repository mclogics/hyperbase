<?php

// Configuration
$app = array();
$app['name'] = 'Orion'; 			// Name of the Application
$app['title'] = 'Orion';			// Web Browser Titkle for the Application
$app['theme']  = 'default';  				// Theme to use for this application
$app['url']['rewrite'] = true; 				// Enable or disbale url rewrite
$app['debug'] = false;						// Set debug mode
$app['mode']['development'] = true;	    	// Set development mode
$app['session']['lifetime'] = 28000;		// Set session lifetime
$app['identity'] = 'Orion';    		// Mail identity of the application
$app['protocol'] = 'https';
$app['i18n'] = array(						// LOCAL SETTINGS
    'charset' => 'UTF-8',
    'language' => 'en_US',
    'timezone' => 'Asia/Calcutta',
    'date_format' => 'M d, Y',
    'time_format' => 'H:i:s'
);


$app['mail'] = array(                                   // Mailing Settings
    'system'    => array(
        'smtp'  => array(								// SMTP settings of protocol System
            'name'              => 'domain.io',		// Domain
            'host'              => 'smtp.gmail.com',    // SMTP Server
            'port'              => 587,                 // SMTP Port Notice port change for TLS is 587
            'connection_class'  => 'login',             // SMTP Class
            'connection_config' => array(
                'username' => 'user@mail.com',		// SMTP Username
                'password' => 'uc61J7c5QulY1dgQnks9',				// SMTP Password
                'ssl'      => 'tls',					// Protocol Security
            ),
        )
    ),
);
$app['mail']['notify'] = true;							// Set this to false to stop all email notification from this application

// Load Required Plugins
$app['plugins'] = array(
    'System_Layout',
    'System_DB',
    'System_DB_Field',
);


$app['layout'] = 'app';

/*
$app['plugin']['system']['db'] = array(
    'connections'  => array(
        'production' => 'mysql://root:root@localhost/orion_application'
    ),
    'default_connection' => 'production'
);*/

