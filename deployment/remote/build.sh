#!/usr/bin/env bash

cp ./deployment/remote/Dockerfile ./

APP_VERSION=${APP_MAIN_VERSION}.$(git rev-list --count HEAD)

docker build -t hub.docker.trinityinteractive.com:5000/wpforever/application:${APP_VERSION} .
docker push hub.docker.trinityinteractive.com:5000/wpforever/application:${APP_VERSION}

export APP_VERSION

rm ./Dockerfile

cp ./deployment/remote/docker-compose.yml ./
cp ./deployment/remote/rancher-compose.yml ./
cp ./deployment/remote/deploy.sh ./
echo "version=${APP_VERSION}" > ./app.vars

sed -i -e "s/{{APP_VERSION}}/${APP_VERSION}/g" ./docker-compose.yml
sed -i -e "s/{{APP_VERSION}}/${APP_VERSION}/g" ./rancher-compose.yml





