#!/usr/bin/env bash

if [ -f /var/www/new.release ]; then
    rm -f /var/www/new.release
    rm -f /var/www/resources/db.version
    rm -f /var/www/resources/config.php
    rm -rf /var/www/resources/data
    rm -f
    cd /var/www
    if [ ! -e /var/www/resources/config.php ]; then
        cp /var/www/deployment/remote/resources/config.template.php /var/www/resources/config.php
        cp -R /var/www/deployment/remote/data /var/www/resources
        sed -i "s:{{db_name}}:wpforever_application:" /var/www/resources/config.php
        sed -i "s:{{db_user}}:root:" /var/www/resources/config.php
        sed -i "s:{{db_password}}:${MYSQL_ROOT_PASSWORD}:" /var/www/resources/config.php
        sed -i "s:{{db_host}}:mysql:" /var/www/resources/config.php
    fi
    sh bin/install.sh wpforever
    rm -rf /var/www/resources/data
fi
/usr/sbin/apache2ctl -D FOREGROUND