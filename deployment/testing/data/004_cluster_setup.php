<?php
try {
    \Model\Application::create(array(
        "id"              => 1,
        "cluster_id"      => 1,
        "user_id"         => 0,
        "name"            => "PLATFORM",
        "platform"        => "Service/Platform",
        "version"         => "1.0",
        "domain"          => "1",
        "data"            => '{"setup_params":{"mysql_root_password":"ACu2aE8fRst5e9ZuAQh","mysql_database":"platform","mysql_user":"wpfpuser","mysql_password":"PSCUhD5o4xJLscXV82","ftp_ip":"45.33.68.134","floating_ips":{"web_lb":{"45.56.108.103":"45.56.108.103"}},"temp_app_domain":"cl56.dev.wpforever.com"},"setup":[{"id":"1e32","type":"environment","links":{"self":"https:\/\/cloud.dev.wpforever.trinityinteractive.com:6466\/v1\/projects\/1a5\/environments\/1e32","account":"https:\/\/cloud.dev.wpforever.trinityinteractive.com:6466\/v1\/projects\/1a5\/environments\/1e32\/account","services":"https:\/\/cloud.dev.wpforever.trinityinteractive.com:6466\/v1\/projects\/1a5\/environments\/1e32\/services","composeConfig":"https:\/\/cloud.dev.wpforever.trinityinteractive.com:6466\/v1\/projects\/1a5\/environments\/1e32\/composeconfig"},"actions":{"remove":"https:\/\/cloud.dev.wpforever.trinityinteractive.com:6466\/v1\/projects\/1a5\/environments\/1e32\/?action=remove","error":"https:\/\/cloud.dev.wpforever.trinityinteractive.com:6466\/v1\/projects\/1a5\/environments\/1e32\/?action=error","addoutputs":"https:\/\/cloud.dev.wpforever.trinityinteractive.com:6466\/v1\/projects\/1a5\/environments\/1e32\/?action=addoutputs","activateservices":"https:\/\/cloud.dev.wpforever.trinityinteractive.com:6466\/v1\/projects\/1a5\/environments\/1e32\/?action=activateservices","deactivateservices":"https:\/\/cloud.dev.wpforever.trinityinteractive.com:6466\/v1\/projects\/1a5\/environments\/1e32\/?action=deactivateservices","exportconfig":"https:\/\/cloud.dev.wpforever.trinityinteractive.com:6466\/v1\/projects\/1a5\/environments\/1e32\/?action=exportconfig"},"name":"PLATFORM","state":"activating","accountId":"1a5","created":"2016-12-07T22:11:18Z","createdTS":1481148678000,"description":null,"dockerCompose":"# LoadBalancer\nloadbalancer:\n  ports:\n    - 80:80\n    - 443:443\n  tty: true\n  labels:\n    io.rancher.scheduler.affinity:host_label: has.web.lb=true\n  links:\n    - phpmyadmin:phpmyadmin\n  image: wpforever\/loadbalancer:1.0\n  stdin_open: true\n  volumes:\n    - \/mnt\/storage\/internal-shared\/loadbalancer\/nginx\/nginx.conf:\/etc\/nginx\/nginx.conf\n    - \/mnt\/storage\/internal-shared\/loadbalancer\/nginx\/conf.d:\/etc\/nginx\/conf.d\n    - \/mnt\/storage\/internal-shared\/loadbalancer\/nginx\/default-html:\/usr\/share\/nginx\/html\n    - \/mnt\/storage\/application:\/opt\n\n# FTP Server\nftp-server:\n  ports:\n    - 21:21\n    - 20:20\n    - 30000-30099:30000-30099\n  image: kauden\/pure-ftpd-mysql\n  stdin_open: true\n  environment:\n    EXTERNALIP: \"45.33.68.134\"\n  labels:\n      io.rancher.scheduler.affinity:host_label: has.ftpserver=true\n  links:\n     - mysql:mysql\n  volumes:\n    - \/mnt\/storage\/:\/ftpdata\n    - \/mnt\/storage\/internal-shared\/pure-ftpd\/mysql.conf:\/etc\/pure-ftpd\/db\/mysql.conf # Need to be pouplated\n    - \/mnt\/storage\/internal-shared\/pure-ftpd\/pure-ftp.pem:\/etc\/ssl\/private\/pure-ftpd.pem # Need to be generated\n    - \/mnt\/storage\/internal-shared\/pure-ftpd\/conf:\/etc\/pure-ftpd\/conf # Need to be generated\n  cpu_shares: 256   #25% or (1024\/100)*25 = 256\n  cpu_quota: 50000  #50% or (100000\/100)*50 = 50000\n  #cpuset: 0,1\n  mem_limit: 204800000\n  memswap_limit: 128000000\n  shm_size: 256000000\n\n# MySQL\nmysql:\n  image: mysql:5.7\n  ports:\n    - 5656:3306\/tcp\n  volumes:\n    - \"\/mnt\/storage\/mysql\/data:\/var\/lib\/mysql\"\n  restart: always\n  labels:\n        io.rancher.scheduler.affinity:host_label: has.mysql=true\n  environment:\n    TERM: \"xterm\"\n    MYSQL_ROOT_PASSWORD: \"ACu2aE8fRst5e9ZuAQh\"\n    MYSQL_DATABASE: \"platform\"\n    MYSQL_USER: \"wpfpuser\"\n    MYSQL_PASSWORD: \"PSCUhD5o4xJLscXV82\"\n  cpu_shares: 256   #25% or (1024\/100)*25 = 256\n  cpu_quota: 50000  #50% or (100000\/100)*50 = 50000\n  #cpuset: 0,1\n  mem_limit: 512000000\n  memswap_limit: 128000000\n  shm_size: 256000000\n\n# PhpMyAdmin\nphpmyadmin:\n  image: dyutiman\/phpmyadmin:5.0\n  container_name: myadmin\n  environment:\n   - UPLOAD_SIZE=1G\n  restart: always\n  cpu_shares: 256   #25% or (1024\/100)*25 = 256\n  cpu_quota: 50000  #50% or (100000\/100)*50 = 50000\n  #cpuset: 0,1\n  mem_limit: 512000000\n  memswap_limit: 128000000\n  shm_size: 256000000\n","environment":null,"externalId":null,"healthState":"healthy","kind":"environment","outputs":null,"previousEnvironment":null,"previousExternalId":null,"rancherCompose":"# LoadBalancer\nhttp-lb:\n  scale: 1\n  retain_ip: true\n\nhttps-lb:\n  scale: 1\n  retain_ip: true\n\nftp-server:\n  scale: 1\n\n\n# MySQL\nmysql:\n  scale: 1\n  metadata:\n    mysqld: |\n      innodb_file_per_table = 1\n      innodb_autoinc_lock_mode=2\n      query_cache_size=0\n      query_cache_type=0\n      innodb_flush_log_at_trx_commit=0\n      binlog_format=ROW\n      default-storage-engine=innodb\n      progress=1\n      external-locking=TRUE\n\n# PhpMyAdmin\nphpmyadmin:\n  scale: 1\n\n# Default Landing Page\nwp-landing:\n  scale: 1\n\n","removed":null,"startOnCreate":true,"transitioning":"yes","transitioningMessage":"In Progress","transitioningProgress":null,"uuid":"e394bea1-3ecc-4e19-b753-cf5ce6c0bc87"}]}',
        "state"           => "active",
        "stack_id"        => "1e32",
        "health"          => "healthy",
        "last_checked"    => 1481148712,
        "is_ready"        => 1,
        "start_at"        => 1481148688
    ));
} catch(\Exception $e) { echo $e->getMessage()."\n"; }


try {
    \Model\Port::register(1, 'mysql', 20000, 20010);

} catch(\Exception $e) { echo $e->getMessage()."\n"; }

try {
    \Model\Service::create(array(
        'id'                => 1,
        'application_id'    => 1,
        'name'              => 'loadbalancer_ips',
        'data'              => '["45.56.108.103"]',
        'stack_id'          => '1s127'
    ));

    \Model\Service::create(array(
        'id'                => 2,
        'application_id'    => 1,
        'name'              => 'phpmyadmin',
        //'data'              => '',
        'stack_id'          => '1s126'
    ));

    \Model\Service::create(array(
        'id'                => 3,
        'application_id'    => 1,
        'name'              => 'mysql_hosts',
        'data'              => '["45.33.68.134"]',
        //'stack_id'          => ''
    ));

    \Model\Service::create(array(
        'id'                => 4,
        'application_id'    => 1,
        'name'              => 'ftp_hosts',
        'data'              => '["45.33.68.134"]',
        //'stack_id'          => ''
    ));

} catch(\Exception $e) { echo $e->getMessage()."\n"; }

