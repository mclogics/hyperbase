<?php
/**
 * Functions.
 *
 * PHP version 5.5.9
 *
 * Some low level internal function available to the framework.
 *
 * @category   Framework
 * @package    Functions
 * @author     Dyutiman Chakraborty <dc@mclogics.com>
 * @copyright  2014 - 2015, HB
 * @license    https://licenses.domain.com/psl-1.0.txt Proprietary Service Licence ver. 1.0
 *
 * @link       https://framework.local
 */

/**
 * Print out an array or object contents in preformatted text
 * Useful for debugging and quickly determining contents of variables.
 *
 * <code>
 *      _debug($arrayOne, $arrayTwo ...., $objectOne, $objectTwo... $arrayN, $objectN);
 * </code>
 *
 * @return void
 */
function _debug()
{
    $objects = func_get_args();
    $content = ''; //"\n<pre>\n";
    foreach ($objects as $object) {
        $content .= print_r($object, true);
    }
    echo $content; //"\n</pre>\n";
    exit;

    return;
}//end _debug()x

function ___debug()
{
    $objects = func_get_args();
    $content = "\n<pre>\n";
    foreach ($objects as $object) {
        $content .= print_r($object, true);
    }
    echo $content;
    echo "\n</pre>\n";
    exit;

    return;
}//end _debug()x

function user($prop=false, $default=0)
{
    if(!isset($_SESSION['user']['id'])) {
        return $default;
    }
    if(isset($_GLOBALS['_wpforever_user'])) {
        $data = $_GLOBALS['_wpforever_user'];
    } else {
        $data = $_GLOBALS['_wpforever_user'] = \Model\User::find($_SESSION['user']['id'])->to_array();
    }
    if($prop==false) {
        return $data;
    }
    return $data[$prop];
}

function site($prop=false)
{
    if(!isset($_GLOBALS['__wp_site'])) {
        $request = \Kernel()->request()->params();
        if ($request['module'] != 'site' || $request['action'] == 'index') {
            return null;
        }
        if (isset($request['item'])) {
            try {
                $site = \Model\Application::find($request['item']);
            } catch (\ActiveRecord\RecordNotFound $e) {
                return null;
            }
        }
        $_GLOBALS['__wp_site'] = $site->to_array();
    }
    if($prop != false) {
        return $_GLOBALS['__wp_site'][$prop];
    }
    return $_GLOBALS['__wp_site'];
}



function __debug($uid, $data)
{
    if ($_SESSION['user']['user']['id'] == $uid) {
        _debug($data);
    }
}

function _debugAB($a, $b, $data)
{
    if ($a == $b) {
        if (is_callable($data)) {
            call_user_func($data);
        } else {
            _debug($data);
        }
    }
}

function sortByOrder($a, $b)
{
    return $a['order'] - $b['order'];
}

function getAdminLoadcssColor($load)
{
    if ($load < 50) {
        return '#ccff99';
    }
    if ($load < 80) {
        return '#ffff99';
    }

    return '#ff9999';
}

/**
 * Returns the instance of Kernel Object.
 *
 * @param array $config Optionally the configuration parameters can be provided.
 *
 * @return \System\Kernel
 */
function Kernel(array $config = array())
{
    return \System\Kernel::getInstance($config);
}//end Kernel()


function	norm_str($string) {
    return	trim(strtolower(
        str_replace('.','',$string)));
}

function	in_array_norm($needle,$haystack) {
    return	in_array(norm_str($needle),$haystack);
}

function	parse_name($fullname) {
    $titles			=	array('dr','miss','mr','mrs','ms','judge');
    $prefices		=	array('ben','bin','da','dal','de','del','der','de','e',
        'la','le','san','st','ste','van','vel','von');
    $suffices		=	array('esq','esquire','jr','sr','2','ii','iii','iv');

    $pieces			=	explode(',',preg_replace('/\s+/',' ',trim($fullname)));
    $n_pieces		=	count($pieces);

    $out = array();

    switch($n_pieces) {
        case	1:	// array(title first middles last suffix)
            $subp	=	explode(' ',trim($pieces[0]));
            $n_subp	=	count($subp);
            for($i = 0; $i < $n_subp; $i++) {
                $curr				=	@trim($subp[$i]);
                $next				=	@trim($subp[$i+1]);

                if($i == 0 && in_array_norm($curr,$titles)) {
                    $out['title']	=	$curr;
                    continue;
                }

                if(!isset($out['first'])) {
                    $out['first']	=	$curr;
                    continue;
                }

                if($i == $n_subp-2 && $next && in_array_norm($next,$suffices)) {
                    if($out['last']) {
                        $out['last']	.=	" $curr";
                    }
                    else {
                        $out['last']	=	$curr;
                    }
                    $out['suffix']		=	$next;
                    break;
                }

                if($i == $n_subp-1) {
                    if(isset($out['last'])) {
                        $out['last']	.=	" $curr";
                    }
                    else {
                        $out['last']	=	$curr;
                    }
                    continue;
                }

                if(in_array_norm($curr,$prefices)) {
                    if($out['last']) {
                        $out['last']	.=	" $curr";
                    }
                    else {
                        $out['last']	=	$curr;
                    }
                    continue;
                }

                if($next == 'y' || $next == 'Y') {
                    if($out['last']) {
                        $out['last']	.=	" $curr";
                    }
                    else {
                        $out['last']	=	$curr;
                    }
                    continue;
                }

                if($out['last']) {
                    $out['last']	.=	" $curr";
                    continue;
                }

                if($out['middle']) {
                    $out['middle']		.=	" $curr";
                }
                else {
                    $out['middle']		=	$curr;
                }
            }
            break;
        case	2:
            switch(in_array_norm($pieces[1],$suffices)) {
                case	TRUE: // array(title first middles last,suffix)
                    $subp	=	explode(' ',trim($pieces[0]));
                    $n_subp	=	count($subp);
                    for($i = 0; $i < $n_subp; $i++) {
                        $curr				=	trim($subp[$i]);
                        $next				=	trim($subp[$i+1]);

                        if($i == 0 && in_array_norm($curr,$titles)) {
                            $out['title']	=	$curr;
                            continue;
                        }

                        if(!$out['first']) {
                            $out['first']	=	$curr;
                            continue;
                        }

                        if($i == $n_subp-1) {
                            if($out['last']) {
                                $out['last']	.=	" $curr";
                            }
                            else {
                                $out['last']	=	$curr;
                            }
                            continue;
                        }

                        if(in_array_norm($curr,$prefices)) {
                            if($out['last']) {
                                $out['last']	.=	" $curr";
                            }
                            else {
                                $out['last']	=	$curr;
                            }
                            continue;
                        }

                        if($next == 'y' || $next == 'Y') {
                            if($out['last']) {
                                $out['last']	.=	" $curr";
                            }
                            else {
                                $out['last']	=	$curr;
                            }
                            continue;
                        }

                        if($out['last']) {
                            $out['last']	.=	" $curr";
                            continue;
                        }

                        if($out['middle']) {
                            $out['middle']		.=	" $curr";
                        }
                        else {
                            $out['middle']		=	$curr;
                        }
                    }
                    $out['suffix']	=	trim($pieces[1]);
                    break;
                case	FALSE: // array(last,title first middles suffix)
                    $subp	=	explode(' ',trim($pieces[1]));
                    $n_subp	=	count($subp);
                    for($i = 0; $i < $n_subp; $i++) {
                        $curr				=	trim($subp[$i]);
                        $next				=	trim($subp[$i+1]);

                        if($i == 0 && in_array_norm($curr,$titles)) {
                            $out['title']	=	$curr;
                            continue;
                        }

                        if(!$out['first']) {
                            $out['first']	=	$curr;
                            continue;
                        }

                        if($i == $n_subp-2 && $next &&
                            in_array_norm($next,$suffices)) {
                            if($out['middle']) {
                                $out['middle']	.=	" $curr";
                            }
                            else {
                                $out['middle']	=	$curr;
                            }
                            $out['suffix']		=	$next;
                            break;
                        }

                        if($i == $n_subp-1 && in_array_norm($curr,$suffices)) {
                            $out['suffix']		=	$curr;
                            continue;
                        }

                        if($out['middle']) {
                            $out['middle']		.=	" $curr";
                        }
                        else {
                            $out['middle']		=	$curr;
                        }
                    }
                    $out['last']	=	$pieces[0];
                    break;
            }
            unset($pieces);
            break;
        case	3:	// array(last,title first middles,suffix)
            $subp	=	explode(' ',trim($pieces[1]));
            $n_subp	=	count($subp);
            for($i = 0; $i < $n_subp; $i++) {
                $curr				=	trim($subp[$i]);
                $next				=	trim($subp[$i+1]);
                if($i == 0 && in_array_norm($curr,$titles)) {
                    $out['title']	=	$curr;
                    continue;
                }

                if(!$out['first']) {
                    $out['first']	=	$curr;
                    continue;
                }

                if($out['middle']) {
                    $out['middle']		.=	" $curr";
                }
                else {
                    $out['middle']		=	$curr;
                }
            }

            $out['last']				=	trim($pieces[0]);
            $out['suffix']				=	trim($pieces[2]);
            break;
        default:	// unparseable
            unset($pieces);
            break;
    }

    return $out;
}

/**
 * @param $template
 * @param array $variables
 * @param string $varAppend
 * @param string $varPrepend
 * @return mixed
 */
function process_template($template, array $variables=array(), $varAppend='${', $varPrepend='}')
{
    foreach($variables as $key => $value) {
        $var = $varAppend . $key . $varPrepend;
        if(stripos($template, $var)!== false) {
            $template = str_replace($var, $value, $template);
        }
    }
    return $template;
}

function isOnline($url){
    $agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";$ch=curl_init();
    curl_setopt ($ch, CURLOPT_URL,$url );
    curl_setopt($ch, CURLOPT_USERAGENT, $agent);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($ch,CURLOPT_VERBOSE,false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch,CURLOPT_SSLVERSION,3);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, FALSE);
    $page=curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    if($httpcode>=200 && $httpcode<=308) return true;
    else return false;
}



if (!function_exists('spyc_load')) {
    /**
     * Parses YAML to array.
     * @param string $string YAML string.
     * @return array
     */
    function spyc_load ($string) {
        return \System\Spyc::YAMLLoadString($string);
    }
}

if (!function_exists('spyc_load_file')) {
    /**
     * Parses YAML to array.
     * @param string $file Path to YAML file.
     * @return array
     */
    function spyc_load_file ($file) {
        return \System\Spyc::YAMLLoad($file);
    }
}

if (!function_exists('spyc_dump')) {
    /**
     * Dumps array to YAML.
     * @param array $data Array.
     * @return string
     */
    function spyc_dump ($data) {
        return \System\Spyc::YAMLDump($data, false, false, true);
    }
}


function websocket_open($url){
    $key=base64_encode(uniqid());
    $query=parse_url($url);

    //___debug($query);

    $header="GET / HTTP/1.1\r\n"
        ."Host: ".$query['host'].":".$query['port']."\r\n"
        ."pragma: no-cache\r\n"
        ."cache-control: no-cache\r\n"
        ."Upgrade: WebSocket\r\n"
        ."Connection: Upgrade\r\n"
        ."Sec-WebSocket-Key: $key\r\n"
        ."Sec-WebSocket-Version: 13\r\n"
        ."\r\n";
    $sp=fsockopen($query['host'],$query['port'], $errno, $errstr,1);
    if(!$sp) die("Unable to connect to server ".$url);
    // Ask for connection upgrade to websocket
    fwrite($sp,$header);
    stream_set_timeout($sp,5);
    $reaponse_header=fread($sp, 1024);
    if(!strpos($reaponse_header," 101 ")
        || !strpos($reaponse_header,'Sec-WebSocket-Accept: ')){
        die("Server did not accept to upgrade connection to websocket == "
            .$reaponse_header);
    }
    return $sp;
}

function websocket_write($sp, $data,$final=true){
    // Assamble header: FINal 0x80 | Opcode 0x02
    $header=chr(($final?0x80:0) | 0x02); // 0x02 binary

    // Mask 0x80 | payload length (0-125)
    if(strlen($data)<126) $header.=chr(0x80 | strlen($data));
    elseif (strlen($data)<0xFFFF) $header.=chr(0x80 | 126) . pack("n",strlen($data));
    elseif(PHP_INT_SIZE>4) // 64 bit
        $header.=chr(0x80 | 127) . pack("Q",strlen($data));
    else  // 32 bit (pack Q dosen't work)
        $header.=chr(0x80 | 127) . pack("N",0) . pack("N",strlen($data));

    // Add mask
    $mask=pack("N",rand(1,0x7FFFFFFF));
    $header.=$mask;

    // Mask application data.
    for($i = 0; $i < strlen($data); $i++)
        $data[$i]=chr(ord($data[$i]) ^ ord($mask[$i % 4]));

    return fwrite($sp,$header.$data);
}

function websocket_read($sp,$wait_for_end=true,&$err=''){
    $out_buffer="";
    do{
        // Read header
        $header=fread($sp,2);
        if(!$header) die("Reading header from websocket failed");
        $opcode = ord($header[0]) & 0x0F;
        $final = ord($header[0]) & 0x80;
        $masked = ord($header[1]) & 0x80;
        $payload_len = ord($header[1]) & 0x7F;

        // Get payload length extensions
        $ext_len=0;
        if($payload_len>125) $ext_len+=2;
        if($payload_len>126) $ext_len+=6;
        if($ext_len){
            $ext=fread($sp,$ext_len);
            if(!$ext) die("Reading header extension from websocket failed");

            // Set extented paylod length
            $payload_len=$ext_len;
            for($i=0;$i<$ext_len;$i++)
                $payload_len += ord($ext[$i]) << ($ext_len-$i-1)*8;
        }

        // Get Mask key
        if($masked){
            $mask=fread($sp,4);
            if(!$mask) die("Reading header mask from websocket failed");
        }

        // Get application data
        $data_len=$payload_len-$ext_len-($masked?4:0);
        $frame_data=fread($sp,$data_len);
        if(!$frame_data) die("Reading from websocket failed");

        // if opcode ping, reuse headers to send a pong and continue to read
        if($opcode==9){
            // Assamble header: FINal 0x80 | Opcode 0x02
            $header[0]=chr(($final?0x80:0) | 0x0A); // 0x0A Pong
            fwrite($sp,$header.$ext.$mask.$frame_data);

            // Recieve and unmask data
        }elseif($opcode<9){
            $data="";
            if($masked)
                for ($i = 0; $i < $data_len; $i++)
                    $data.= $frame_data[$i] ^ $mask[$i % 4];
            else
                $data.= $frame_data;
            $out_buffer.=$data;
        }

        // wait for Final
    }while($wait_for_end && !$final);

    return $out_buffer;
}