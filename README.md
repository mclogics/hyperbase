### Instructions for Building Development Copy ###
The following instruction is for creating a working development copy on your local machine to work with. 
Please do not use the dev copy building instructions in production environment, as its not secure.

To install on you local machine, you will need the following software:
* Vagrant
* VirtualBox (or any other provider supported by vagrant)

Once the above dependencies are met, login to your terminal and run the following commend:
 
```
git clone ssh://git@bitbucket.source.logicalclouds.com:7999/hyp/web-application.git \
    && cd web-application \
    && vagrant up
```

The installation is tested on Ubuntu & Mac, but should work properly on any machine which run vagrant.
Please remember to add your public SSH key to this repository, in order for the above commend to work.


### Documentations ###
Coming Soon...

