<?php

$system = array();

// Directories.
$system['dir']['root']   = '/';
$system['dir']['config'] = $system['dir']['root'];
$system['dir']['lib']    = $system['dir']['root'] . 'library';

// Full root paths.
$system['path']['root']    = __DIR__;
if (DEPLOYMENT_MODE == SAAS || DEPLOYMENT_MODE == CONTAINER) {
    $system['path']['config']  = __DIR__ . '/' . strtolower(APP_DOMAIN) . '/config.php';
} else {
    $system['path']['config']  = __DIR__ . '/resources';
}
$system['path']['lib']     = $system['path']['root'] . $system['dir']['lib'];
$system['path']['default'] = $system['path']['root'] . '/defaults';

return array('system' => $system);
