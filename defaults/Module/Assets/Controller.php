<?php
namespace Module\Assets;
use System, System\Exception;

/**
 * Assets Module
 * 
 */
class Controller extends System\Module\ControllerAbstract
{

    public function loadAction(System\Request $request)
    {
        $file_path = realpath(__DIR__ . '/../../../application/themes/'.$this->kernel->config('app.theme').'/'.$request->param('segment').'/' . str_replace('_', '/', $request->param('file') . '.' . $request->param('format')));
        if($request->param('format') == 'js') {
            $mine = 'application/javascript';
        }
        if($request->param('format') == 'css') {
            $mine = 'text/css';
        }
        if(!isset($mine)) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mine = finfo_file($finfo, $file_path);
            finfo_close($finfo);
        }
        header('Content-Type: ' . $mine);
        readfile($file_path);
        $this->view->stopRender();
    }

}