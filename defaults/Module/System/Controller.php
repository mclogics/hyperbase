<?php
namespace Module\System;
use System, System\Exception;

/**
 * Assets Module
 * 
 */
class Controller extends System\Module\ControllerAbstract
{
    /**
     * Index
     * @method GET
     */
    public function indexAction(System\Request $request)
    {
    	throw new Exception\FileNotFound("Invalid Request");
    }
    
    public function logoutAction(System\Request $request)
    {
    	$this->kernel->session()->destroy();
        $this->kernel->redirect('/');
    }


    public function sendmailAction(System\Request $request)
    {
        if(!$request->isCli()) {
            return false;
        }
        set_time_limit(0);
        \App\Mail::send($request->param('item'));
        exit;
    }

}