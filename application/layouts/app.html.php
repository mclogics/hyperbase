<?php
    try {
        $user = \Model\User::find($_SESSION['user']['id']);
    } catch(\ActiveRecord\RecordNotFound $e) {
        include_once __DIR__ . '/app.error.html.php';
        exit;
    }
    if(user('user_type') == USER_TYPE_ADMIN || user('user_type') == USER_TYPE_CLIENT) {
        include_once __DIR__ . '/app.admin.html.php';
        //include_once __DIR__ . '/app.client.html.php';
        exit;
    }
