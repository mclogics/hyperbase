<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Basic Page Needs
  ================================================== -->
    <meta charset="utf-8">
    <title>HyperBase 3.0 | Login</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
  ================================================== -->


    <link href="/assets/login/style.css" rel="stylesheet" type="text/css" />

    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>

<div class="container">
    <div style="margin-top:5%" class="flat-form">
        <ul class="tabs">
            <li>
                <a href="#login" class="active">Login</a>
            </li>
            <li>
                <a href="#reset">Reset Password</a>
            </li>
        </ul>
        <div id="login" class="form-action show">
            <h1>HyperBase 3.0</h1>
            <p>
                Please login with your email address and password.
            </p>
            <form class="login-form" action="/" method="post">
                <ul>
                    <li>
                        <input type="text" name="auth[username]" placeholder="Email" />
                    </li>
                    <li>
                        <input type="password" name="auth[password]" placeholder="Password" />
                    </li>
                    <li style="margin-top:20px;">
                        <input type="submit" value="Login" class="button" />
                    </li>
                </ul>
            </form>
        </div>

        <!--/#register.form-action-->
        <div id="reset" class="form-action hide">
            <h1>Reset Password</h1>
            <p>
                To reset your password enter your email and your birthday
                and we'll send you a link to reset your password.
            </p>
            <form>
                <ul>
                    <li>
                        <input type="text" placeholder="Email" />
                    </li>
                    <li style="margin-top:20px;">
                        <input type="submit" value="Send" class="button" />
                    </li>
                </ul>
            </form>
        </div>
        <!--/#register.form-action-->
    </div>

    <div class="copyright"> Copyright &copy; <?=date('Y')?> <a target="_blank" href="http://www.logicalclouds.com">LogicalClouds</a>. All Rights Reserved <br/>
        version : <?=\Kernel()->getAppVersion()?></div>


</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script src="/assets/login/script.js"></script>
</body>
</html>


