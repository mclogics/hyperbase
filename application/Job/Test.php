<?php

namespace Job;

final class Test extends \System\Job
{
    protected function run()
    {
        $this->setLogMessage('This is a demo message');
        sleep(1);
        $this->setLogMessage('This is a success test', 'success');
        sleep(1);
        $this->setLogMessage('This is a warning test', 'warning');
        sleep(1);
        $this->setLogMessage('Staring a ping on ' . $this->param('ping_host') . '...');

        $this->autoCompletePercentage(function() {
            $this->exec('ping -c '.$this->param('count', 10).' '.$this->param('ping_host'));
            //\Model\Server::first()->tailExec('ping -c '.$this->param('count', 10).' '.$this->param('ping_host'), $this->job->log());
        }, 80, 1);

        $this->setLogMessage('This is an error test', 'error');

        //$this->exec('ping -c '.$this->param('count', 10).' '.$this->param('ping_host'));
        //\Model\Server::first()->tailExec('ping -c '.$this->param('count', 10).' '.$this->param('ping_host'), $this->job->log());
        $this->setLogMessage('Ping on ' . $this->param('ping_host') . ' has ended after '.$this->param('count', 10).' request');
        return true;
    }
}